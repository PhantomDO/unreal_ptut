// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class PTUT_ONLINETarget : TargetRules
{
	public PTUT_ONLINETarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "PTUT_ONLINE" } );
	}
}
