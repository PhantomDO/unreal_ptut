// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class PTUT_ONLINEEditorTarget : TargetRules
{
	public PTUT_ONLINEEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "PTUT_ONLINE" } );
	}
}
