// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "MyPlayerActions.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyPlayerController.generated.h"

UCLASS()
class PTUT_ONLINE_API AMyPlayerController : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPlayerController();
private:
	float PitchValue = 0;
	float YawValue = 0;
	FVector CurrentVelocity;
	FVector AxisVector;
	FVector PosToLook;
	float TimeLimit;
	float Speed;
	bool bIsSprinting = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Amount);
	void MoveRight(float Amount);

	void RotateForward(float Amount);
	void RotateRight(float Amount);
	
	void RotateDir(float Value);

	void TakeObject();
	void SprintAction();
	void SprintEnd();

	class UMyPlayerActions * PlayerActions;
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool CanSprint;
	UPROPERTY(EditAnywhere)
	float MinLookVectorLenght = 0.25f;
	UPROPERTY(EditAnywhere)
	float WalkSpeed = 500.0f, SprintSpeed = 1500.0f, TimeLimitValue = 2.0f;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
