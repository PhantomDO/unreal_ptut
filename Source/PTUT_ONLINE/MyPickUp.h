// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"

#include "MyPickUp.generated.h"

UCLASS()
class PTUT_ONLINE_API AMyPickUp : public AActor
{
	GENERATED_BODY()

private:
	bool bHolding;
	bool bGravity;

	FRotator ControlRotation;

	UPROPERTY()
	TArray<ACharacter*> TheCharacters;
	ACharacter* MyCharacter;

	FVector ForwardVector;

public:
	// Sets default values for this actor's properties
	AMyPickUp();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent * MyMesh;

	UPROPERTY(EditAnywhere)
	USceneComponent *HoldingComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector InitialLocalVelocity;

	UPROPERTY(EditAnywhere)
	float shootForce = 100000.0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsLaunch = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* LaunchingPlayer;

	UFUNCTION()
	void RotateActor();

	UFUNCTION()
	void PickUp();

	UFUNCTION()
	ACharacter* CheckTheNearestCharacter();

	UFUNCTION()
	void ProcessHoldingComp(ACharacter* Char);

};
