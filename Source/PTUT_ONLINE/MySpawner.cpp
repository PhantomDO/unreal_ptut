// Fill out your copyright notice in the Description page of Project Settings.

#include "MySpawner.h"

// Sets default values
AMySpawner::AMySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>("MyMesh");

	MyMesh->SetSimulatePhysics(false);
	SetRootComponent(MyMesh);
}

// Called when the game starts or when spawned
void AMySpawner::BeginPlay()
{
	Super::BeginPlay();
	
	if (spawnLocation.IsZero())
		spawnLocation = this->MyMesh->GetComponentLocation();

	if (rotator.IsZero())
		rotator = this->MyMesh->GetComponentRotation();
}

// Called every frame
void AMySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMySpawner::Spawn()
{
	if (ToSpawn)
	{
		UWorld* world = GetWorld();
		if (world)
		{
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;

			TheActorSpawn = world->SpawnActor<AActor>(ToSpawn, spawnLocation, rotator, spawnParams);
		}
	}
}

