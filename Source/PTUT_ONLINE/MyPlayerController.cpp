// Fill out your copyright notice in the Description page of Project Settings.
#include "MyPlayerController.h"
#include "GameFramework/Actor.h"
#include "Classes/Components/InputComponent.h"
#include "Classes/GameFramework/FloatingPawnMovement.h"
#include "Camera/CameraComponent.h"
#include "Classes/Components/StaticMeshComponent.h"	
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "DrawDebugHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Runtime/Core/Public/Math/Quat.h"
// Sets default values
AMyPlayerController::AMyPlayerController()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Set this pawn to be controlled by the lowest-numbered player
	//AutoPossessPlayer = EAutoReceiveInput::Disabled;

	PlayerActions = CreateDefaultSubobject<UMyPlayerActions>("PlayerActions");

	PlayerActions->HoldingComponent->RelativeLocation.X = 50.0f;
	PlayerActions->HoldingComponent->SetupAttachment(RootComponent);
	PlayerActions->HoldingComponent->bEditableWhenInherited = true;
}

// Called when the game starts or when spawned
void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();

}

void AMyPlayerController::MoveForward(float Amount)
{
	CurrentVelocity.X = FMath::Clamp(Amount, -1.0f, 1.0f);
}

void AMyPlayerController::MoveRight(float Amount)
{
	CurrentVelocity.Y = FMath::Clamp(Amount, -1.0f, 1.0f);
}

void AMyPlayerController::TakeObject()
{
	PlayerActions->OnAction();
}

void AMyPlayerController::SprintAction()
{
	if (TimeLimit >= TimeLimitValue && CanSprint)
	{
		Speed = SprintSpeed;
		bIsSprinting = true;
	}

	return;
}

void AMyPlayerController::SprintEnd()
{
	if (TimeLimit >= TimeLimitValue || TimeLimit <= 0)
	{
		Speed = WalkSpeed;
		bIsSprinting = false;
	}

	return;
}

void AMyPlayerController::RotateForward(float Amount)
{
	PitchValue = -Amount;
	//UE_LOG(LogTemp, Warning, TEXT("Axis Vector : %f"), PitchValue);
}

void AMyPlayerController::RotateRight(float Amount)
{
	YawValue = Amount;
	//UE_LOG(LogTemp, Warning, TEXT("Axis Vector : %f"), YawValue);
}

void AMyPlayerController::RotateDir(float Value)
{
	AxisVector = FVector(PitchValue * 90, YawValue * 90, 0);

	FRotator Rot = FRotationMatrix::MakeFromX(AxisVector).Rotator();

	//UE_LOG(LogTemp, Warning, TEXT("Axis Vector = X: %f, Y: %f, Z: %f"), AxisVector.Y, AxisVector.X, AxisVector.Z);

	//DrawDebugSphere(GetWorld(), AxisVector, 25, 10, FColor::Emerald, false, .1);

	if (AxisVector.Size() > MinLookVectorLenght)
	{
		//APlayerController* pController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		//pController->SetControlRotation(Rot);
		SetActorRotation(Rot);
	}
}

// Called every frame
void AMyPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Handle rotation of the player
	{
		RotateDir(0);
	}
	
	// Reload the sprint skills
	{
		//UE_LOG(LogTemp, Warning, TEXT("v : %f"), TimeLimit);

		if (TimeLimit <= 0)
			SprintEnd();

		if (bIsSprinting && TimeLimit >= 0)
			TimeLimit -= TimeLimitValue * DeltaTime;

		if (!bIsSprinting && TimeLimit <= TimeLimitValue)
			TimeLimit += TimeLimitValue * DeltaTime;
	}

	// Handle movement based on our "MoveX" and "MoveY" axes
	{
		if (!CurrentVelocity.IsZero())
		{
			FVector NewLocation = GetActorLocation() + (CurrentVelocity * Speed * DeltaTime);
			SetActorLocation(NewLocation);
		}
	}
}

// Called to bind functionality to input
void AMyPlayerController::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMyPlayerController::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMyPlayerController::MoveRight);
	PlayerInputComponent->BindAction("TakeObject", IE_Pressed, this, &AMyPlayerController::TakeObject);
	PlayerInputComponent->BindAction("SprintAction", IE_Pressed, this, &AMyPlayerController::SprintAction);
	PlayerInputComponent->BindAction("SprintAction", IE_Released, this, &AMyPlayerController::SprintEnd);

	PlayerInputComponent->BindAxis("RotateForward", this, &AMyPlayerController::RotateForward);
	PlayerInputComponent->BindAxis("RotateRight", this, &AMyPlayerController::RotateRight);
}

