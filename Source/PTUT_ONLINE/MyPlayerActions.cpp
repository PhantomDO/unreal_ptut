// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerActions.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UMyPlayerActions::UMyPlayerActions()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	HoldingComponent = CreateDefaultSubobject<USceneComponent>("HoldingComponent");
		
	CurrentItem = NULL;


	// ...
}


// Called when the game starts
void UMyPlayerActions::BeginPlay()
{
	Super::BeginPlay();
	// ...

}

void UMyPlayerActions::OnAction()
{
	if (CurrentItem)
	{
		ToggleItemPickUp();
	}
}

void UMyPlayerActions::ToggleItemPickUp()
{
	//UE_LOG(LogTemp, Warning, TEXT("Take"));
	if (CurrentItem)
	{
		bHoldingItem = !bHoldingItem;
		CurrentItem->PickUp();
		
		if (!bHoldingItem)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Relache"));
			CurrentItem = NULL;
		}
	}
}


// Called every frame
void UMyPlayerActions::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//UE_LOG(LogTemp, Warning, TEXT("I'm Tick"));
	Start = GetOwner()->GetActorLocation();
	End = ((GetOwner()->GetActorForwardVector() * 200.f) + Start);

	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(GetOwner());

	DrawDebugSphere(GetWorld(), HoldingComponent->GetComponentLocation(), 50, 10, FColor::Blue, false, .1);

	DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, DeltaTime);

	if (!bHoldingItem)
	{
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, CollisionParams, DefaultResponseParams))
		{
			//UE_LOG(LogTemp, Warning, TEXT("AHAHA : %s"), *Hit.GetActor()->GetClass()->GetName());
			if (Hit.GetActor()->GetClass()->IsChildOf(AMyPickUp::StaticClass()))
			{
				CurrentItem = Cast<AMyPickUp>(Hit.GetActor());
			}
		}
		else
		{
			CurrentItem = NULL;
		}
	}
	// ...
}

