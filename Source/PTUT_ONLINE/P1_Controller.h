// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyPlayerController.h"
#include "P1_Controller.generated.h"

/**
 * 
 */
UCLASS()
class PTUT_ONLINE_API AP1_Controller : public AMyPlayerController
{
	GENERATED_BODY()

	AP1_Controller();
};
