// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Public/CollisionQueryParams.h"
#include "MyPickUp.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectBase.h"
#include "GameFramework/Actor.h"
#include "MyPlayerActions.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PTUT_ONLINE_API UMyPlayerActions : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMyPlayerActions();
	
	UPROPERTY(EditAnywhere)
	class USceneComponent * HoldingComponent;

	UPROPERTY(EditAnywhere)
	class AMyPickUp* CurrentItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bHoldingItem;

	FVector HoldingComp;
	FRotator LastRotation;

	FVector Start;
	FVector ForwardVector;
	FVector End;

	FHitResult Hit;

	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

	void OnAction();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void ToggleItemPickUp();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	
};
