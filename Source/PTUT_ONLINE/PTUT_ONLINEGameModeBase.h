// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PTUT_ONLINEGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PTUT_ONLINE_API APTUT_ONLINEGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
