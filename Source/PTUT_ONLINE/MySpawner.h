// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyPickUp.h"
#include "GameFramework/Actor.h"
#include "Classes/Components/StaticMeshComponent.h"
#include "MySpawner.generated.h"

UCLASS()
class PTUT_ONLINE_API AMySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMySpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MyMesh;

	UPROPERTY(EditAnywhere)
	FVector spawnLocation;

	UPROPERTY(EditAnywhere)
	FRotator rotator;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> ToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* TheActorSpawn;

	UFUNCTION(BlueprintCallable)
	void Spawn();
};
