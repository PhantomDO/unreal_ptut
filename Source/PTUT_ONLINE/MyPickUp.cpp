// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPickUp.h"
#include "GameFramework/Character.h"
#include "Classes/Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Runtime/Core/Public/Misc/App.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "EngineUtils.h"
#include <string>

// Sets default values
AMyPickUp::AMyPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>("MyMesh");

	MyMesh->SetSimulatePhysics(false);
	SetRootComponent(MyMesh);

	

	bHolding = false;
	bGravity = true;
}

template<typename T>
void FindAllActors(UWorld* World, TArray<T*>& Out)
{
	for (TActorIterator<T> It(World); It; ++It)
	{
		if (!Out.Contains(*It))
			Out.Add(*It);
	}
}

// Called when the game starts or when spawned
void AMyPickUp::BeginPlay()
{
	Super::BeginPlay();
	MyMesh->SetSimulatePhysics(false);

	FindAllActors(GetWorld(), TheCharacters);

	ProcessHoldingComp(CheckTheNearestCharacter());
	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerController::StaticClass(), MyCharacter);

	/*MyCharacter[0] = UGameplayStatics::GetPlayerCharacter(this, 0);
	MyCharacter[1] = UGameplayStatics::GetPlayerCharacter(this, 1);
	MyCharacter[2] = UGameplayStatics::GetPlayerCharacter(this, 2);
	MyCharacter[3] = UGameplayStatics::GetPlayerCharacter(this, 3);*/
}

// Called every frame
void AMyPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMyPickUp::RotateActor()
{
	ControlRotation = GetWorld()->GetFirstPlayerController()->GetControlRotation();
	SetActorRotation(FQuat(ControlRotation));
}

void AMyPickUp::PickUp()
{
	bHolding = !bHolding;
	bGravity = !bGravity;
	MyMesh->SetEnableGravity(bGravity);
	MyMesh->SetSimulatePhysics(bHolding ? false : true);
	MyMesh->SetCollisionEnabled(bHolding ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryAndPhysics); 

	if (bHolding && HoldingComp)
	{
		ProcessHoldingComp(CheckTheNearestCharacter());
		MyMesh->AttachToComponent(HoldingComp, FAttachmentTransformRules::KeepWorldTransform);
		SetActorLocationAndRotation(HoldingComp->GetComponentLocation(), HoldingComp->GetComponentRotation());
	}

	if (!bHolding)
	{
		LaunchingPlayer = HoldingComp->GetAttachmentRootActor();
		//UE_LOG(LogTemp, Warning, TEXT("Actuel launcher : %s"), *LaunchingPlayer->GetFullName());
		MyMesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		ForwardVector = MyCharacter->GetActorForwardVector();
		
		MyMesh->AddForce((ForwardVector + InitialLocalVelocity) * shootForce * MyMesh->GetMass());
		IsLaunch = true;
	}
}

ACharacter* AMyPickUp::CheckTheNearestCharacter()
{
	ACharacter* TheNeededOne = MyCharacter;
	bool FirstOne = false;
	float test = 0;

	for (auto& Character : TheCharacters)
	{
		if (!FirstOne)
		{
			test = FVector::Dist(GetActorLocation(), Character->GetActorLocation());
			TheNeededOne = Character;
			FirstOne = !FirstOne;
		}
		else
		{
			if (test > FVector::Dist(GetActorLocation(), Character->GetActorLocation()))
			{
				test = FVector::Dist(GetActorLocation(), Character->GetActorLocation()); 
				TheNeededOne = Character;
			}
		}

		UE_LOG(LogTemp, Warning, TEXT("Char : %s, Dist : %f"), *Character->GetName(), test);
	}

	return TheNeededOne;
}

void AMyPickUp::ProcessHoldingComp(ACharacter* Char)
{
	MyCharacter = Char;

	UE_LOG(LogTemp, Warning, TEXT("%s"), *MyCharacter->GetName());

	TArray<USceneComponent*> Components;
	MyCharacter->GetComponents(Components);

	if (Components.Num() > 0)
	{
		for (auto& Comp : Components)
		{
			if (Comp->GetName() == "HoldingComponent")
			{
				HoldingComp = Cast<USceneComponent>(Comp);
			}
		}
	}
}

