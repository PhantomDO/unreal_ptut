// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyPlayerController.h"
#include "P4_Controller.generated.h"

/**
 * 
 */
UCLASS()
class PTUT_ONLINE_API AP4_Controller : public AMyPlayerController
{
	GENERATED_BODY()

		AP4_Controller();
};
